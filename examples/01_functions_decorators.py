"""
Functions. Generators.
"""
import operator
from collections.abc import Iterable
from functools import reduce
from itertools import islice


b = 42

def test(a, seq=None):
    """
    This is a test.
    """
    if not seq:
        seq = []
    seq.append(a)
    seq.append(b)
    return seq

# comprehensions
# [x*y for x in range(10) for y range(10) if x > 5]
# {k: v for enumerate(range(10))}
# (x*x for x in range(10) if x > 5)


def test_scope(a, b, c=10, *args, **kwargs):
    print(locals())


def adder(a):
    def inner(b):
        return a + b
    return inner


def is_positive(fn):
    def inner(n):
        # preconditions check
        if n <= 0:
            raise ValueError('Argument must be positive')
        # do actual work
        result = fn(n)
        # postconditions check
        # ...
        return result
    return inner


def bounded(a, b):
    """ Decorator with parameters. """
    def wrapper(fn):
        def inner(n):
            if n < a or n > b:
                raise ValueError('Not bounded')
            return fn(n)
        return inner
    return wrapper


@bounded(1, 100)
def factorial(n):
    return reduce(operator.mul, range(1, n), 1)


def countdown(n):
    while n > 0:
        yield n
        n -= 1


def countdown2(n):
    """ Countdown, which can be reset. """
    while n > 0:
        a = yield n
        if a is not None:
            n = a
        else:
            n -= 1


def fib(n):
    """ Recursive fibonacci. Inefficient! """
    if n < 2:
        return 1
    else:
        return fib(n - 2) + fib(n - 1)


def xfib():
    """ Infinite sequence of the fibonacci numbers. """
    a = b = 1
    while True:
        yield a
        a, b = b, a + b


list(islice(xfib(), 10))


def fac(n):
    """ Recursive factorial. """
    if n <= 1:
        return 1
    else:
        return n * fac(n - 1)


def fac_iter(n):
    """ Iterative factorial. """
    a = 1
    for x in range(1, n):
        a *= x
    return a


nested = [1, [2, 3, (4, 5)], 6]  # -> [1, 2, 3, 4, 5, 6]


def flatten(seq):
    """
    Flatten nested sequence.

    >>> flatten([1, [2, 3, (4, 5)], 6])
    [1, 2, 3, 4, 5, 6]

    """
    res = []
    for e in seq:
        if isinstance(e, Iterable):  # hasattr(e, '__iter__')
            res.extend(flatten(e))
        else:
            res.append(e)
    return res


# NB: yield from is not available in Python 2
def xflatten(seq):
    """
    Flatten nested sequence. Implemented as generator.

    >>> flatten([1, [2, 3, (4, 5)], 6])
    [1, 2, 3, 4, 5, 6]

    """
    for e in seq:
        if isinstance(e, Iterable):
            yield from xflatten(e)
        else:
            yield e


tree = (((1, 2), (3, 4)), ((5, 6), (7, 8)))


def is_tree(n):
    return isinstance(n, tuple)


def sumtree(t):
    l, r = t
    suml = sumtree(l) if is_tree(l) else l
    sumr = sumtree(r) if is_tree(r) else r
    return suml + sumr
