def fn():
    return 42


def gen():
    print('before yield')
    yield 42
    print('after first yield')
    yield 1
    print('after second yield')
    # raise StopIteration


def countdown(n):
    res = []
    while True:
        if n <= 0:
            break
        res.append(n)
        n -= 1
    return res


def xcountdown(n):
    while n > 0:
        yield n
        n -= 1


def mul2(seq):
    res = []
    for x in seq:
        res.append(x * 2)
    return res


def xmul2(seq):
    for x in seq:
        yield x * 2
