class Money:
    def __init__(self, amount, currency):
        self.amount = amount  # decimal.Decimal
        self.currency = currency

    def test(self):
        print(id(self))

    def __str__(self):
        return str(self.amount) + ' ' + self.currency

    def __repr__(self):
        return "Money({}, '{}')".format(self.amount, self.currency)

    def __eq__(self, other):
        return type(self) == type(other) and self.amount == other.amount and self.currency == other.currency

    def __add__(self, other):
        if self.currency == other.currency:
            return Money(self.amount + other.amount, self.currency)
        else:
            raise ValueError('')


from collections import namedtuple

m = (10, 'UAH')
m[0]
amount, currency = m

Money = namedtuple('Money', ('amount', 'currency'))


# public
# protected
# private

class Carousel(object):
    def __init__(self, vehicles):
        self._vehicles = vehicles  # protected
        self.__current_vehicle = None  # private

    def setCurrentVehicle(self):
        pass

    def __iter__(self):
        return (x for x in self.vehicles)


class Vehicle:
    # __dict__
    speed = 42
    def __init__(self, data):
        # self.__dict__
        self.data = data
        self.speed = 42

    def __getattr__(self, name):  # fallback
        return self.data[name]

    def __getattribute__(self, name):  # .
        # custom logic
        return object.__getattribute__(name)

# vehicle = Vehicle(load_from_json('ussr:T-34.json'))
# vehicle.data['guns']


# functor
class Counter:
    def __init__(self):
        self.__n = 0

    def __call__(self):
        self.__n += 1

    def call_count(self):
        return self.__n

# sorted(key=lambda x: int(x))

carousel = Carousel([])

# 1
hasattr(carousel, '__iter__')

# 2
from collections.abc import Iterable, Mapping
isinstance(carousel, Iterable)


class Entity:
    def __init__(self, position):
        self.position = position


class PhysicsEntity:
    def __init__(self, weight):
        self.weight = weight


class MovableEntity(Entity):
    def __init__(self, position, max_speed):
        super().__init__(position)
        self.max_speed = max_speed

    def moveToPoint(self, point):
        pass


class Vehicle(MovableEntity, PhysicsEntity):
    def moveToPoint(self, point):
        super().moveToPoint(point)
